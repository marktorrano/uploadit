<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::resource('/uploads', 'UploadController');
Route::resource('/tags', 'TagController');
Route::resource('/images', 'ImageController');
Route::delete('/delete-image-tag', 'ImageTagController@deleteImageTag');
Route::post('/add-tag-to-image', 'ImageTagController@addTagToImage');
Route::get('/search-tags', 'TagController@searchTags');
Route::post('/store-image', 'TagController@storeImage');
