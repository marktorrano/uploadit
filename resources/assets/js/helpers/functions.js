/**
 *  example 1: empty(null)
 *  returns 1: true
 *  example 2: empty(undefined)
 *  returns 2: true
 *  example 3: empty([])
 *  returns 3: true
 *  example 4: empty({})
 *  returns 4: true
 *  example 5: empty({'aFunc' : function () { alert('humpty'); } })
 *  returns 5: false
 */
function empty (mixedVar) {
    let undef;
    let key;
    let i;
    let len;
    let emptyValues = [undef, null, false, 0, '', '0'];

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixedVar === emptyValues[i]) {
            return true
        }
    }

    if (typeof mixedVar === 'object') {
        for (key in mixedVar) {
            if (mixedVar.hasOwnProperty(key)) {
                return false
            }
        }
        return true
    }

    return false
}

function getModal(title = '', body = '', cancelAction = null, proceedAction = null, isLargeModal = null ) {

    let modal = $('.modal.modal-default');

    if (isLargeModal) {
        $('.modal-dialog', modal).addClass('modal-lg');
    } else {
        $('.modal-dialog', modal).removeClass('modal-lg');
    }

    $('.modal-header', modal).removeClass('btn-default btn-danger btn-primary btn-success btn-info btn-warning btn-danger btn-link');

    $('.modal-title', modal).empty().html(title);
    $('.modal-body', modal).empty().html(body);
    $('.modal-footer button.back', modal)
        .show()
        .text('Back')
        .unbind('click')
        .on('click', function() {
            if (!empty(cancelAction)) {
                cancelAction();
            }
    });
    $('.modal-footer button.proceed', modal)
        .show()
        .text('Proceed')
        .unbind('click')
        .on('click', function() {
            if (!empty(proceedAction)) {
                proceedAction();
            }
    });

    return modal;
}
