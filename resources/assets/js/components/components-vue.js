let Component = Vue.extend({
    mixins: [commonVue]
});

const events = new Component({
    methods: {}
});
Vue.component('image-list', {
    data() {
        return {
            dataSet: [],
            tagList: [],
            search: ''
        }
    },
    ready() {
        let self = this;
        this.getData();

    },
    methods: {

        addTagToImage(image_id, tag_id, callBack = null) {
            $.ajax({
                url: baseUrl + "/api/add-tag-to-image",
                type: "POST",
                data: {
                    image_id: image_id,
                    tag_id: tag_id
                }
            }).done(function() {
                if (!empty(callBack)) {
                    callBack();
                }
            });
        },

        setAutocomplete(e) {
            let self = this;
            let input = e.target;
            let image_id = $(input).attr('data-image-id');
            $(input).autocomplete({
                minLength: 1,
                source: function (request, response) {
                    $.ajax({
                        url: baseUrl + "/api/search-tags",
                        data: {name: $(input).val()},
                        dataType: "json",
                        type: "GET",
                        success: function (data) {
                            response($.map(data, function (obj) {
                                return {
                                    name: obj.name,
                                    id: obj.id
                                };
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    let callBack = function() {
                        $(input).val('');
                        self.getData();
                    };
                    self.addTagToImage(image_id, ui.item.id, callBack)
                }
            }).autocomplete("instance")._renderItem = function (ul, tag) {
                return $("<li></li>")
                    .data("item.autocomplete", tag)
                    .append("<span data-id='" + tag.id + "'>" + tag.name + "</span>")
                    .appendTo(ul);
            };
        },

        addTag(e, image_id = null) {
            let self = this;
            let name = $(e.target).val();

            if (empty(name)) {
                return;
            }

            $.ajax({
                url: baseUrl + "/api/tags",
                type: "POST",
                data: {
                    name: name
                }
            }).done(function (resp) {
                let tag_id = (!empty(resp.id)) ? resp.id : resp;
                let callBack = function () {
                    $(e.target).val('');
                    self.getData();
                };
                self.addTagToImage(image_id, tag_id, callBack);
            });
        },

        showAddTagInput(e) {
            let self = this;
            let icon = e.target;
            let parent = $(icon).closest('.container');

            $(parent).find('input').removeClass('hide')
                .focus()
                .on('blur', function () {
                    $(this).addClass('hide');
                });
        },

        removeTag(image_id, tag_id) {
            let self = this;
            let modal = $('.modal.modal-delete').modal();
            let proceedButton = $('button.proceed', modal);

            $(modal).on('shown.bs.modal', function () {
                $(proceedButton).focus();
            });

            $(proceedButton, modal).unbind('click').on('click', function () {
                $.ajax({
                    url: baseUrl + "/api/delete-image-tag",
                    type: "DELETE",
                    data: {
                        image_id: image_id,
                        tag_id: tag_id
                    }
                }).done(function () {
                    toastr.success('Tag removed');
                    self.getData();
                    modal.modal('hide');
                });
            });
        },

        getData(tags = []) {
            let self = this;

            let tag_ids = [];
            $.each(tags, function (k, val) {
                tag_ids.push(val.id);
            });

            $.ajax({
                url: baseUrl + "/api/images",
                type: "GET"
            }).done(function (response) {
                self.dataSet = response;
                events.retouchPopOverEvents();
            })
        }
    }
});

Vue.component('upload', {
    data() {
        return {
            dataSet: [],
        }
    },
    ready() {
        let self = this;
        events.setAddDropzoneEvents('image/*', 1);
        events.retouchPopOverEvents();

        let input = $("#add-tags");
        $(input).autocomplete({
            minLength: 1,
            source: function (request, response) {
                $.ajax({
                    url: baseUrl + "/api/search-tags",
                    data: {name: $("#add-tags").val()},
                    dataType: "json",
                    type: "GET",
                    success: function (data) {
                        response($.map(data, function (obj) {
                            return {
                                name: obj.name,
                                id: obj.id
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                let item = {name: ui.item.name, id: ui.item.id};
                self.dataSet.push(item);
                self.dataSet = events.filterDuplicates(self.dataSet);
                $(input).val('');
                return false;
            }
        }).autocomplete("instance")._renderItem = function (ul, tag) {
            return $("<li></li>")
                .data("item.autocomplete", tag)
                .append("<span data-id='" + tag.id + "'>" + tag.name + "</span>")
                .appendTo(ul);
        };
    },
    methods: {

        removeTag(index) {
            this.dataSet = events.removeTagFromList(index, this.dataSet);
        },

        upload() {
            let self = this;
            let image_id = $('.dz-preview.dz-complete').attr('data-id');
            let tags = this.dataSet;
            let tag_ids = [];

            if (empty(image_id)) {
                toastr.warning('Image required.');
                return;
            }

            $.each(tags, function (k, val) {
                tag_ids.push(val.id);
            });

            $.ajax({
                url: baseUrl + "/api/store-image",
                type: "POST",
                data: {
                    image_id: [image_id],
                    tag_ids: tag_ids
                }
            }).done(function () {
                Dropzone.forElement(".dropzone").emit("resetFiles");
                self.dataSet = [];
                toastr.success('Successfully uploaded.');
            }).fail(function (err) {
                console.log(err);
                toastr.error('Oops something went wrong while uploading photo.');
            })
        },

        addToTagList(name) {
            this.dataSet.push(name);
        },

        addTag(e, value = null) {
            let self = this;
            let name = !empty(value) ? value : $(e.target).val();

            if (empty(name)) {
                return;
            }

            $.ajax({
                url: baseUrl + "/api/tags",
                type: "POST",
                data: {
                    name: name
                }
            }).done(function (id) {
                self.addToTagList({name: name, id: id});
                self.dataSet = events.filterDuplicates(self.dataSet);
                $('input[name=name]').val('');
            });
        },
    }
});
