let commonVue = {
    methods: {
        /**
         * Removes tag from the list
         * @param index
         * @param list
         */
        removeTagFromList: function (index, list) {
            list.splice(index, 1);
            return list;
        },

        /**
         * Filters duplicates from given object or array
         * @param list List to be filtered
         * @returns {Array} Filtered list
         */
        filterDuplicates: function (list) {
            let result = [];
            $.each(list, function (i, e) {
                let matchingItems = $.grep(result, function (item) {
                    return item.name === e.name && item.label === e.label;
                });
                if (matchingItems.length === 0) {
                    result.push(e);
                }
            });

            return result;
        },
        /**
         * Initialize dropzone instance
         * @param fileTypes list of file types
         * @param maxNumberOfFiles Allowed number of files to be uploaded in 1 dropzone instance
         */
        setAddDropzoneEvents: function (fileTypes = 'image/*', maxNumberOfFiles = 10) {
            let url = baseUrl + "/api/uploads";
            $('.dropzone').each(function() {
                let dropzone = this;
                try {
                    $(dropzone).dropzone({
                        url: url,
                        addRemoveLinks: true,
                        acceptedFiles: fileTypes,
                        maxFiles: maxNumberOfFiles,
                        timeout: 0,
                        success: function success(file, id) {
                            id = $.trim(id.replace(/[\t\n]+/g, ' '));
                            toastr['info']('File was uploaded');
                            $(file.previewElement).attr("data-id", id);
                        },
                        removedfile: function removedfile(file) {
                            let id = $(file.previewElement).attr("data-id");
                            $.ajax({
                                url: url + "/" + id,
                                type: "DELETE",
                                success: function success(id) {
                                    toastr['info']('File was deleted');
                                },
                                fail: function fail() {
                                }
                            });
                            let _ref;
                            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                        },
                        init: function init() {
                            this.on("maxfilesexceeded", function (file) {
                                toastr['warning']("Number of max files exeeded");
                                let _ref = void 0;
                                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                            });
                            this.on('resetFiles', function () {
                                if (this.files.length !== 0) {
                                    for (i = 0; i < this.files.length; i++) {
                                        this.files[i].previewElement.remove();
                                    }
                                    this.files.length = 0;
                                }
                            });
                        }
                    });
                } catch (err) {
                    console.log(err);
                }
            });
        },

        /**
         * Re apply popover events
         * @param option Additional options, refer to toastr docs
         * @param callBack Function to be executed
         */
        retouchPopOverEvents: function (option = {}, callBack = null) {
            let options = {
                trigger: "hover",
                container: 'body',
                placement: 'top',
            };
            $.extend(options, option);
            setTimeout(function(){
                $('[data-toggle="popover"]').popover(options);
                if (!empty(callBack)) {
                    callBack();
                }
            }, 200);
        },
    }
};

