@extends('layouts.app')
@push('styles')
    <style>
        .tags-list {
            min-height: 5em
        }

        .tags {
            margin: auto 1em;
            padding: 1px 0 1px 5px;
        }

        .tags i {
            font-size: .8em
        }

        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            min-height: 20em;
        }

        .card .frame {
            min-height: 15em;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

        .container {
            padding: 2px 16px;
        }

        .card .container {
            width: 80%
        }
    </style>
@endpush

@section('content')

    <div class="container" style="width: 90vw" id="uploads-vue">
        <image-list inline-template>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group col-md-3">
                        <div class="tag-list">
                        </div>
                        <input type="text"
                               class="form-control"
                               v-model="search"
                               placeholder="Search by tags"
                               autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3" v-for="(index, image) in dataSet | filterBy search" track-by="$index">
                    <div class="card">
                        <div class="frame">
                            <img :src="image.url" alt="Avatar" style="width:100%">
                        </div>
                        <div class="container">
                            <div class="form-group">
                                <input class="form-control hide add-tag input-sm"
                                       type="text"
                                       name="name"
                                       autocomplete="off"
                                       placeholder="Add Tag"
                                       @keyup.enter="addTag($event, image.id)"
                                       @keydown="setAutocomplete"
                                       :data-image-id="image.id"
                                />
                            </div>
                            <label for="">Tags <i class="fa fa-plus-circle text-success cursor-pointer"
                                                 @click="showAddTagInput($event)"
                                >
                                </i></label>
                            <div class="col-md-12">
                                <div class="col-md-4" v-for="(key, tags) in image.tags" track-by="$index">
                                    <span class="tags cursor-pointer"
                                          @click="removeTag(image.id, tags.id)"
                                          data-toggle="popover"
                                          data-content="Click to remove tag"
                                    >
                                            @{{ tags.name }}
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </image-list>
        @include('modals.delete')
        @include('modals.default')
    </div>
@endsection
@push('scripts')
    <script>
        const baseUrl = '<?= url('/')?>';
        Dropzone.autoDiscover = false;

        const uv = new Component({

            el: '.app',
            data: {},
            /**
             * ready
             * On page load function
             */
            ready: function () {


            },
            /**
             * List of methods
             */
            methods: {}
        });

    </script>
@endpush