@extends('layouts.app')
@push('styles')
    <style>
        .tags-list {
            min-height: 5em
        }

        .tags {
            margin: auto 1em;
            border: 1px solid #aaa;
            padding: 1px 0 1px 5px;
        }

        .tags i {
            margin: auto 3px
        }
    </style>
@endpush

@section('content')

    <div class="container" style="width: 90vw" id="uploads-vue">

        <upload inline-template>
            <div class="col-md-4 col-md-offset-4">
                <h3>Upload a Photo</h3>
                <form action="" class="dropzone" id="addDropzone">
                    <div class="dz-message" data-dz-message>
                        <span>Drop files here or click to upload</span>
                    </div>
                </form>
                <div class="tags-list col-md-12">
                    <h4>Tags</h4>
                    <span v-for="(index, tag) in dataSet" track-by="$index"
                          class="tags"
                    >   <span>@{{ tag.name }}
                            <i class="fas fa-times cursor-pointer"
                               @click="removeTag(index)"
                            ></i>
                        </span>
                    </span>
                </div>
                <div class="form-group">
                    <label>Add Tags
                        <i class="fa fa-info-circle text-info"
                           data-toggle="popover"
                           data-content="Press enter to add tag"
                        ></i>
                    </label>
                    <input class="form-control" type="text" name="name" autocomplete="off"
                           id="add-tags"
                           @keyup.enter="addTag"
                    />
                </div>
                <span class="btn btn-success"
                    @click="upload"
                >Upload</span>

            </div>
        </upload>

        @include('modals.default')
    </div>
@endsection
@push('scripts')
    <script>
        const baseUrl = '<?= url('/')?>';
        Dropzone.autoDiscover = false;

        const uv = new Component({

            el: '#uploads-vue',
            data: {},
            /**
             * ready
             * On page load function
             */
            ready: function () {


            },
            /**
             * List of methods
             */
            methods: {

            }
        });

    </script>
@endpush