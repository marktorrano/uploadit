<div class="modal fade modal-default" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary back" data-dismiss="modal">
                    Back
                </button>
                <button type="button" class="btn btn-success proceed">
                    Proceed
                </button>
            </div>
        </div>
    </div>
</div>