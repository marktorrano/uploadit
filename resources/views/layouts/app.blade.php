<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>UploadIt</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ url('/public/css/app.css') }}" rel="stylesheet">
    <link href="{{ url('/resources/assets/css/dropzone/5.4.0/dropzone.min.css') }}" rel="stylesheet">
    <link href="{{ url('/resources/assets/css/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ url('/resources/assets/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    @stack('styles')
    <style>
        .modal { overflow: auto !important; }
        .cursor-pointer:hover {cursor: pointer}
    </style>
</head>
<body id="app-layout" class="app">

    <nav class="navbar navbar-default navbar-static-top">
        @include('layouts.nav')
    </nav>
    @yield('content')
    <!-- JavaScripts -->
    <script type="text/javascript" src="{{url("/resources/assets/js/vendor/jquery/3.3.1/jquery.js")}}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript" src="{{url("/resources/assets/js/common-vue.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/vendor/vue.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/vendor/dropzone.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/vendor/jquery-ui.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/vendor/bootstrap/3.3.7/bootstrap.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/helpers/functions.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/vendor/toastr.min.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/vendor/jquery.autocomplete.min.js")}}"></script>
    <script type="text/javascript" src="{{url("/resources/assets/js/components/components-vue.js")}}"></script>
    @stack('scripts')
</body>
</html>