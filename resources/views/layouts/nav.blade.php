<div class="container">
    <div class="navbar-header">
        <!-- Collapsed Hamburger -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <!-- Branding Image -->
        <a class="navbar-brand" href="{{url('/')}}">
            UploadIt
        </a>
    </div>

    <div class="collapse navbar-collapse " id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav ">
            <li><a href="{{ url('/upload') }}">Upload</a></li>

        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            <li><a href="{{ url('/search') }}">Search</a></li>
        </ul>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('a.change-password').on('click', function () {
                this.preventDefault;
                let content = '';
                content += '<form>';
                content += '<label >New Password</label>';
                content += '<input type="password" class="form-control" name="password"/>';
                content += '<label>Confirm New Password</label>';
                content += '<input type="password" class="form-control" name="password-confirm"/>';
                content += '</form>';
                let modal = getModal('Change Password', content).modal();

                let callBack = function () {
                    let password = $('input[name=password]', modal).val();
                    let password_confirm = $('input[name=password-confirm]', modal).val();

                    if (password !== password_confirm) {
                        toastr['warning']('Passwords does not match.');
                        return;
                    }
                    if (password.length < 6) {
                        toastr['warning']('Password must have at least 6 characters.');
                        return;
                    }

                    let url = baseUrl + "/api/user/" + user_id;
                    $.ajax({
                        url: url,
                        type: "PUT",
                        data: {
                            password: password,
                            initialpassword: '',
                        }
                    }).done(function () {
                        toastr['success']('Password has been changed');
                        modal.modal('hide');
                    }).fail(function () {
                        toastr['error']('Oops something went wrong while changing password.');
                    });
                };

                $('button.proceed', modal).on('click', function () {
                    callBack();
                });

            });
        });
    </script>
@endpush