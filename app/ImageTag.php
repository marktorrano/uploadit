<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageTag extends Model
{
    protected $table = 'image_tag';

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }
}
