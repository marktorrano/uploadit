<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

}
