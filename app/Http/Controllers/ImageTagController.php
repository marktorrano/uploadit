<?php

namespace App\Http\Controllers;

use App\ImageTag;
use App\Image;
use App\Tag;
use Illuminate\Http\Request;

class ImageTagController extends Controller
{

    /**
     * Removes tag from image
     *
     * @param Request $request
     * @throws \Exception
     */
    public function deleteImageTag(Request $request)
    {
        $imageTag = ImageTag::where('image_id', $request->image_id)
            ->where('tag_id', $request->tag_id)
            ->first();

        $imageTag->delete();
    }

    public function addTagToImage(Request $request)
    {
        $image = Image::where('id', $request->image_id)->first();
        $image->tags()->syncWithoutDetaching([$request->tag_id]);
    }
}
