<?php

namespace App\Http\Controllers;

use App\Image;
use App\Upload;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('upload.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        //
        return view('upload.search');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $target_dir = "uploads/files/";
        $fileName =  trim(addslashes($_FILES['file']['name']));
        $filename = str_replace(' ', '_', $fileName);
        $fileErrMsg = $_FILES['file']['error'];

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $newName = basename($filename, ".$ext") . date('mdyhis') . ".$ext";
        $moveResult = move_uploaded_file($_FILES["file"]["tmp_name"], resource_path($target_dir) . $newName);

        if ($moveResult != true) {
            echo $fileErrMsg;
        } else {
            $image = new Image;
            $image->url = "resources/$target_dir$newName";
            $image->save();
        }

        echo $image->id;
        exit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        //
        $upload = Upload::find($id);
        $upload->delete();
    }
}
