<?php

namespace App\Http\Controllers;

use App\Image;
use App\ImageTag;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{

    public function storeImage(Request $request)
    {
        $image = Image::where('id', $request->image_id)->first();

        $image->tags()->sync($request->tag_ids);
    }

    public function searchTags(Request $request)
    {
        $tags = Tag::where('deleted_at', null)
            ->where('name', 'like', "$request->name%")
            ->get();

        return response()->json($tags->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return integer id
     */
    public function store(Request $request)
    {
        $tag = Tag::where('deleted_at', null)
            ->where('name', $request->name)
            ->first();
        if (!empty($tag)) {
            return response()->json(['name' => $tag->name, 'id' => $tag->id]);
        }
        $tag = new Tag;
        foreach ($request->all() as $field => $value) {
            $tag->$field = $value;
        }
        $tag->save();
        return $tag->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
